# 文件服务器
![](https://img-blog.csdnimg.cn/img_convert/438a4abb5903249e1959e617d43893e7.png)
### 🏆介绍
是一款开源的文件系统，特别适合以文件为载体的在线服务，如图片、视频、文档等等。功能主要包括：文件存储、文件分组、自动分片、文件访问（文件上传、文件下载）等。属于应用级文件系统，不是通用的文件系统，基于http协议，所以跨语言。
### 🎊功能
1. 大文件自动分片
2. 文件储存
3. 文件分组
4. 文件上传
5. 文件下载
6. web上传
### 🎨运行
#### ✨配置
```yml
server:
  port: 8082

# 服务基础
burybell:
  storage:
    name: storage-1
    base: d:/tmp/storage
    buffer: 1024
    slice: 209715200
# spring
spring:
  servlet:
    multipart:
      max-request-size: 4000MB
      max-file-size: 40001MB

```
1. burybell.storage.base 文件保存根路径
2. burybell.storage.buffer 文件保存缓存区大小
3. burybell.storage.buffer 文件分片阈值
4. server.port 服务端口号 
   

####🎉运行
#####打包
```bash
mvn clean package -DskipTests
```
#####运行
```bash
java -jar xxx.jar
```
### ⛳快速体验
https://gitee.com/burybell/burybell-storage/releases

### 🥎截图预览
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/022133_2531826e_2260882.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/022214_ca870c4b_2260882.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/022242_1c2868a5_2260882.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/022259_d72c7ede_2260882.png "屏幕截图.png")
