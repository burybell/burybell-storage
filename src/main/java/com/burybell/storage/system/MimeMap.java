package com.burybell.storage.system;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class MimeMap {

    private static Map<String,String> map = new HashMap<String,String>();
    static {
        Properties properties = new Properties();
        try {
            properties.load(MimeMap.class.getResourceAsStream("/mime.properties"));
            properties.keySet().forEach(key -> {
                if (key instanceof String){
                    map.put(key.toString(),properties.getProperty(key.toString()));
                }
            });
            properties.clone();
        } catch (IOException e) {
            map.clear();
        }
    }

    /**
     * 获取Mime属性
     * @param suffix
     * @return
     */
    public String getMime(String suffix){
        String mime = map.get(suffix);
        if (mime == null) {
            return "application/octet-stream";
        } else {
            return mime;
        }
    }

}
