package com.burybell.storage.system.bean;

import lombok.Data;

@Data
public class GetBean {
    private String scene;
    private Integer year;
    private Integer month;
    private Integer day;
    private String sid;
}
