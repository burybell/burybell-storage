package com.burybell.storage.system.bean;

import com.alibaba.fastjson.JSON;
import com.burybell.storage.domain.Storage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BurstInputStream extends InputStream implements Closeable {

    private int seek = 0;
    private int slice = 0;
    private Storage storage = null;
    private final List<FileInputStream> streams = new ArrayList<FileInputStream>();

    public BurstInputStream(String path,String name) {

        try {
            // 获取文件基本信息
            BufferedReader reader = new BufferedReader(new FileReader(new File(path,name)));
            try {
                int contextLength = Integer.parseInt(reader.readLine().replace("Content-Length:",""));
                char[] chars = new char[contextLength];
                int read = reader.read(chars);
                storage = JSON.parseObject(new String(chars,0,read),Storage.class);
                if (reader.readLine().equals("")) {
                    slice = Integer.parseInt(reader.readLine().replace("Slice:",""));reader.close();
                    // 获取所有的流完毕
                    for (int i = 0; i < slice; i++) {
                        streams.add(new FileInputStream(new File(path,name + (i == 0 ?"" : "."+i ))));
                    }
                    FileInputStream inputStream = streams.get(0);
                    while (true) {
                        int ch = inputStream.read();
                        if (ch == '\r') {
                            int chn = inputStream.read();
                            int chr = inputStream.read();
                            int chnc = inputStream.read();
                            if (chn == '\n' && chr == '\r' && chnc == '\n') {
                                break;
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int read() throws IOException {
        if (seek < streams.size()) {
            int read = streams.get(seek).read();
            if (read == -1) {
                seek++;
                if (seek < streams.size()) {
                    return streams.get(seek).read();
                } else {
                    return -1;
                }
            } else {
                return read;
            }
        } else {
            return -1;
        }
    }

    public Storage getStorage() {
        return storage;
    }

    @Override
    public void close() throws IOException {
        for (FileInputStream stream : streams) {
            stream.close();
        }
    }

    @Override
    public String toString() {
        return "BurstInputStream{" +
                "seek=" + seek +
                ", slice=" + slice +
                ", storage=" + storage +
                ", streams=" + streams +
                '}';
    }
}