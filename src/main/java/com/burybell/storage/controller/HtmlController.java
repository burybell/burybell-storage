package com.burybell.storage.controller;

import com.burybell.storage.system.StorageTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class HtmlController {

    @Resource
    private StorageTemplate storageTemplate;

    @GetMapping("/upload")
    public String upload(Model model) {

        List<String> scenes = storageTemplate.scenes();
        model.addAttribute("scenes",scenes);

        return "upload";
    }


}
