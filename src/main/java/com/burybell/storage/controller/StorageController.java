package com.burybell.storage.controller;

import com.burybell.storage.domain.SystemStatus;
import com.burybell.storage.system.StorageTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class StorageController {

    @Resource
    private StorageTemplate storageTemplate;

    @GetMapping("/")
    public SystemStatus system() {
        return new SystemStatus(storageTemplate.getName(),storageTemplate.totalSize(),"you know,for storage!");
    }
}
