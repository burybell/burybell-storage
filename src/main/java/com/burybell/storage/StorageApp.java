package com.burybell.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动文件服务器 8082
 */
@SpringBootApplication
public class StorageApp {
    public static void main(String[] args) {
        SpringApplication.run(StorageApp.class,args);
    }
}
