package com.burybell.storage.utils;

import com.burybell.storage.system.MimeMap;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

public class ResponseUtils {

    public static void doDownloadNameSuffix(MimeMap mimeMap, HttpServletResponse response, String name, String suffix){
        response.setContentType(mimeMap.getMime(suffix.toLowerCase(Locale.ROOT)));
        try {
            response.setHeader("Content-Disposition", "attachment;fileName="+ URLEncoder.encode(name,"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void doViewNameSuffix(MimeMap mimeMap, HttpServletResponse response, String name, String suffix){
        response.setContentType(mimeMap.getMime(suffix.toLowerCase(Locale.ROOT)));
        try {
            response.setHeader("Content-Disposition", "inline;fileName="+ URLEncoder.encode(name,"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
