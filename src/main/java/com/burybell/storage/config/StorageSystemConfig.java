package com.burybell.storage.config;

import com.burybell.storage.system.MimeMap;
import com.burybell.storage.system.StorageTemplate;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Data
@Configuration
@ConfigurationProperties(prefix = "burybell.storage",ignoreUnknownFields = true)
public class StorageSystemConfig {

    private String name;
    private String base;
    private Integer buffer;
    private Integer slice;


    @Bean
    public StorageTemplate storageTemplate() {
        log.info("文件系统初始化完毕");
        log.info("name: " + name);
        log.info("base: " + base);
        log.info("buffer: " + buffer);
        log.info("sliceMin: " + slice);
        return new StorageTemplate(name,base,buffer,slice);
    }

    @Bean
    public MimeMap mimeMap() {
        return new MimeMap();
    }
}
