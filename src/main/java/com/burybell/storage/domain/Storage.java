package com.burybell.storage.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Storage {

    private Long sid; // 文件id
    private String definition; // 文件名称
    private Long size; // 文件大小
    private String suffix; // 文件类型

    private String scene; // 文件场景
    private Integer year; // 年
    private Integer month; // 月
    private Integer day; // 日

}
