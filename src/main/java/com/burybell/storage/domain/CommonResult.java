package com.burybell.storage.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CommonResult {

    private Integer code;
    private String massage;
    private Object data;

    public CommonResult(Integer code,String massage) {
        this(code,massage,null);
    }
}
